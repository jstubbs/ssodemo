#!/bin/bash

echo /dev/null > /wso2am-1.7.0/nohup.out
export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64/jre/
nohup /wso2am-1.7.0/bin/wso2server.sh &>> /wso2am-1.7.0/nohup.out &
sleep infinity