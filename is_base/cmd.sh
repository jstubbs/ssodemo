#!/bin/bash

echo /dev/null > /wso2is-5.0.0/nohup.out
export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64/jre/
nohup /wso2is-5.0.0/bin/wso2server.sh &>> /wso2is-5.0.0/nohup.out &
sleep infinity