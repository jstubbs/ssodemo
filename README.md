# SSO demo with WSO2 Identity Server and API Manager #

## Start Up ##
1. start up a mysql database and load it with data.

# run mariadb instance:
docker run -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=welcome1 mariadb:5.5

# check connectivity:
mysql -u root -h 172.17.42.1 -pwelcome1

# fix mariadb binlog format errors:
MariaDB [mysql]> SET GLOBAL binlog_format = 'ROW';

# load data
mysql -u root -h 172.17.42.1 -p < ssodemo/mysql_ssodemo/mysql_alldbs_0728_0900.sql


2. Run the containers with docker-compose:

docker-compose up -d


